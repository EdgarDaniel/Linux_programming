#!/bin/bash

if [[ $1 == "monitor" ]]; then
	echo "Monitoring devices"
	udevadm monitor --udev

	echo "Monitoring specific devices"
	udevadm monitor --kernel --property --subsystem-match=usb
fi

cat << end

Example of udev rule for specific wirelss mouse connected (locate in /etc/udev/rules.d/example.rules):

	SUBSYSTEM=="usb",\
	ATTR{idVendor}=="062a",\
	ATTR{idProduct}=="4102",\
	ACTION=="add",\
	ENV{DISPLAY}=":0.0",\
	#ENV{XAUTHORITY}=="/run/user/1000/.mutter-Xwaylandauth.R4BC60", \
	RUN+="/home/edgarvillanueva/usbplugged.sh"
end

