/*
 		NOMBRE: Edgar Daniel Magallon Villanueva
 *		PROPOSITO: Insertar los datos iniciales (del registro de cuenta) del atleta.
 *		En la tabla medidas antropometricas se ingrsa la talla y el peso, sin embargo,
 *		la fecha de registro por el momento no se le asigna (hasta despues).
 *
 *		LOGICA: Los valores que se requieren en este SP se distribuyen a lo largo de estas 3 tablas y 
 *		con el orden que se muestra:
 *
 *			a)Info Personal: Nombre y apellidos, fecha de modificacion
 *			b)En DatosEntrevista: se require el uuid de Info personal
 *			c) Atletas: Requiere el genero, correo,fecha de nacimiento,password,uuidDatosEntrevista. 
 *			El campo activo y fecha de registro se registran automaticamente (campo defauly).
 *			d)Medidas Antropro: uuidAtleta, peso y talla(estatura)
 *			e) Codigo Activacion:
 *
 *		FECHA CREACION: Apr 18, 2022
 *		CREADOR: Edgar Daniel Magallon Villanueva
 *		RETURN: Status y mensaje de error/exito
 * 
 * 
 *		FECHA MODIFICACION:
 *		DESARROLLADOR:
 * 	
 *		REVISION: 1
 */
use INAF
DELIMITER  $$
