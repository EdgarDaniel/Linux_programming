import java.util.Scanner;

public class Main {
   
   public static void main(String[] args) throws Exception {
      System.out.println("Hello From Java!");
      Thread.sleep(5000);
      System.out.println("Goodbye");

      Scanner sc = new Scanner(System.in);
      System.out.println("Type something: ");
      String line = sc.nextLine();
      System.out.println("Typed: "+line);

   }

}
