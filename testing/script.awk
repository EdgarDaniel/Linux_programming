#! /usr/bin/awk

{
   if($1 ~ ".*>.*") print ","$2","$3 
   else if($2 ~ ".*>.*") print $1",,"$3 
   else if($3 ~ ".*>.*") print $1","$2"," 
   else print
}

