#!/bin/bash

socat TCP4-LISTEN:3300,reuseaddr
coproc nc -l localhost 3300

while read -r cmd; do
	
	case $cmd in
	d) date ;;
	q) break ;;
	*) echo 'What?'
	esac
	
done <&"${COPROC[0]}" >&"${COPROC[1]}"

kill "$COPROC_PID"
