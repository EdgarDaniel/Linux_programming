#!/bin/bash

. colors.sh

tmpDir=$HOME/tmp/

function canceled_removeFiles {

    echo "Process canceled"
    echo "Removing files of $tmpDir ..."
    rm ${tmpDir}*
    exit
}

trap canceled_removeFiles SIGINT

successfully(){

    echo -e "${GREEN}The programs has been successfully${NC}"
    touch ${tmpDir}fileSuccessfull.data
    
}

trap successfully EXIT

echo "Use of trap"

echo "Dir to test: $tmpDir"

read -p "Num of files to create: " num

for (( ; num>=1; num-- ))
do
    echo "Creating file test_$num.txt: "
    touch ${tmpDir}test_$num.txt
    sleep 1
done

