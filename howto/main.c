#include <stdio.h>
#include <unistd.h>

int main() {
    pid_t pid = getpid();
    int value = 5;

    do {
        printf("value: %d\naddress: %p\npid: %d\n", value, &value, pid);
    } while (getchar() != -1);

    return 0;
}
