#!/usr/bin/bash

cat << EOF
	For more info about test operators, run:
		help test in bash
		man or run-help in zsh
EOF

echo "Go to: https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html"

. ../modules/colors.sh

set -e

if (	
	read -sn1 -p "Running inside a subshell from an if statement";echo
	echo "Subshell: $BASH_SUBSHELL"
	read -p "Type a program to test if exists: " program
	command -v $program 
)
then
	echo "The program exists"
else
	echo "The program does not exist"
fi

echo "Using operators"

printf "Using the ${GREEN}'or' ${NC}operator\n"

read -p "Type a number, can be 1 or 10: " num

if test $num -eq 1 -o $num -eq 10 ; then
    echo "The number is correct"
else
    echo -e "${RED}The number is not correct${NC}"
fi

read -p "Type a number between 10 and 50: " num

if [ $num -ge 10 -a $num -le 50 ]
then
    echo "The range is correct"
else
    echo -e "${RED}The number is not correct${NC}"
fi


read -p "Type one of next names: Edgar Blas, Juan Dios, Daniel,Edgar*, Luis*: " num

if [[ "$num" = "Juan Dios"  ||  $num = "Edgar Blas" || $num = "Daniel" || $num =~ ^Edgar* || $num = "Luis" ]]; then
    echo "The name is correct"
else
    echo -e "${RED}The name is not correct${NC}"
fi

if [ -n "$num" ]; then
    echo "The string $num is greater than 0"
fi

if [[ -z $num ]]; then
    printf "${YELLOW}The string is empty${NC}\n"
fi

echo "using the or like this: answer =~ ^y(es)?"

read -p "You wanna continue? y/Y/yes/Yes: " ans

if [[ $ans =~ ^y|Y(es)?$ ]]
then
    echo "Continuing"

    echo "Eval:  [[ aaaa == "aaaa" ]]?"
    [[ aaaa == "aaaa" ]] && echo yes

    a='aaaa'
    echo "Eval: [[ aaaa == "$a" ]]?"
    [[ aaaa == "$a" ]] && echo yes

    a='a*'
    echo  "Eval:  [[ aaaa == \"$a\" ]]?"
    [[ aaaa == "$a" ]] && echo yes

    a='a*'
    echo "Eval:  [[ aaaa == $a ]]?"
    [[ aaaa == $a ]] && echo yes


fi

