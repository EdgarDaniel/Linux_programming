import hid

# this needs pip3 install hid
vid = 0x054c	# Change it for your device
pid = 0xce6	# Change it for your device

with hid.Device(vid, pid) as h:
	print(f'Device manufacturer: {h.manufacturer}')
	print(f'Product: {h.product}')
	print(f'Serial Number: {h.serial}')
