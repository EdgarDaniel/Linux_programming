#!/bin/bash

echo "Beep system with 'echo -ne \007'"
read -sn 1 -ep "Any key to sound"
echo -ne '\007'

read -sn 1 -ep "Sound by speakers?[yY/nN]" opt

if [[ "$opt" =~ (y|Y) ]];then
	speaker-test -t sine -f 700 -l 1
	#$(speaker-test -t sine -f 1000 )& pid=$! ; sleep 0.1s ; kill -9 $pid
fi
	
read -sn 1 -ep "Sound by sox(with alsa)?[yY/nN]" opt
	
if [[ "$opt" =~ (y|Y) ]];then
	play -q -n synth 0.1 sin 880
fi
