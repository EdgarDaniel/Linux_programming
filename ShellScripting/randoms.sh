#!/bin/bash

r="urandom"

if [[ $# == 0 ]]; then
	size=8
else
		size=$1
		
		if [[ "$2" == "r" ]];then
		
			r="random"
			
		fi
			
fi

echo "Randoms with /dev/$r" 

echo "The size is: $size"


tr -dc A-Za-z0-9_ < /dev/$r | head -c $size | xargs
