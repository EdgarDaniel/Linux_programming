#include <stdio.h>

int main(int argc, char *argv[]) {
  puts("Hi, testing repo v2");
  if (argc > 1)
    printf("Hello World: %s\n", argv[1]);
  else
    printf("Hello World: fuzz\n");
  return 0;
}
