#!/bin/bash

echo "Text to manipulate: 'Hello Tom, how are you?'"
echo "Hello Tom , how are you?" | awk '{$2="Daniel"; $5="old are"; print $0 }'
read -sn 1 -ep "Continue..."
echo "Hello Tom , how are you?" | awk '{$2="guy"; $4="who"; print $0 }'


echo -e "\n------String converter------"
echo "I will convert your input of file: '$1'."
echo "Converting..."
sleep 0.5
read -sn 1 -ep "Finished. Press any key to see results"

awk -F: -f $(pwd)/awk_script $1


echo "I will convert your input of file: /etc/passwd."
echo "Converting..."
sleep 0.5
read -sn 1 -ep "Finished. Press any key to see results"

awk -f awk_passUser /etc/passwd

#"\$2>0 && \$1==\"$1\"" https://stackoverflow.com/questions/2451635/howto-pass-a-string-as-parameter-in-awk-within-bash-script


