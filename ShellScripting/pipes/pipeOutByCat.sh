#!/bin/bash

cat << EOF
Can get output using (cat is optional):
	1) ./pipeOutByCat.sh cat <<< "Math Shadows\nSynyster Gates"
	2) echo "Text" | ./pipeOutByCat.sh cat
	3) ./pipeOutByCat.sh cat < ./content.txt 
EOF

#read out
if [[ $1 == "cat" ]]; then
	out=$(cat)
	echo -e "The output is \n$out"
	exit
fi

if [ -p /dev/stdin ]; then
	echo "Data was piped to this script!"
	# If we want to read the input line by line
	while IFS= read line; do
		echo "Line: ${line}"
		done
		# Or if we want to simply grab all the data, we can simply use cat instead
		# cat
		else
			echo "No input was found on stdin, skipping!"
			# Checking to ensure a filename was specified and that it exists
			if [ -f "$1" ]; then
				echo "Filename specified: ${1}"
				echo "Doing things now.."
				else
					echo "No input given!"
			fi
fi

