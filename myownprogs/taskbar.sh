#!/bin/bash

echo "Progress bar with echo"

echo -ne '#####                     (33%)\r'
sleep 1
echo -ne '#############             (66%)\r'
sleep 1
echo -ne '#######################   (100%)\r'
echo -ne '\n'

printf "Progress bar with %s and unicode char='\\\\u2587': \n" "printf"

printf "\u2587\u2587                     (33%%)\r"
sleep 1
printf "\u2587\u2587\u2587               (66%%)\r"
sleep 1
printf "\u2587\u2587\u2587\u2587         (100%%)\r"
printf "\nFinished\n"

printf "Task bar with spinner: "
i=1
sp="/-\|"
echo -n ' '
while true
do
    printf '\b%.1s' "$sp"
    sp=${sp#?}${sp%???}
#    printf "\b${sp:i++%${#sp}:1}"
done
