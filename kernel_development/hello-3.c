#include <linux/module.h> /*Needed by all modules*/
#include <linux/kernel.h> // Needed for KERN_INFO

/*
 *		 
 */

/*
 * This function is executed when the module is loaded by 'insmod,modprobe'
 */


static int hello3_data __initdata = 3;

int __init  initM(void){
   printk(KERN_WARNING "hello-3.ko Hello World %d.\n",hello3_data);

   //Return 0=module loaded succesfully
   //Return -number: failed to insert module (only prints initM messages)
   //Return number>=1: Works but show warnings
   return 0;
}

/*
 * This function is executed just before removing the module: 'rmmod'
 */
void __exit exitM(void){
   printk(KERN_WARNING "hello-3.ko Goodbye world 3.\n");
}

module_init(initM);
module_exit(exitM);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This is the module 3");
MODULE_AUTHOR("Edgar Daniel Magallon");
/*  
 *  This module uses /dev/testdevice.  The MODULE_SUPPORTED_DEVICE macro might
 *  be used in the future to help automatic configuration of modules, but is 
 *  currently unused other than for documentation purposes.
 */
//It seems this was removed
//MODULE_SUPPORTED_DEVICE("/dev/vda");
