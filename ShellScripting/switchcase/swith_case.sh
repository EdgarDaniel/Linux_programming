#! /bin/bash

echo "Uso del case: "

echo "Actions: "
printf "\t[1] Show current directory\n"
printf "\t[2] Show status of file/folder\n"
printf "\t[sS_tart] Run command\n"
printf "\t[kK_ill] Kill process PID\n"
printf "\t[eE_xit] Exit\n"

read -p "Choose an option: " opc

case $opc in
    1)
	ls 
	;;
    2)
	read -p "Type the directory: " dir
	stat $dir
	;;
    
    [sS]tart)
	read -p "Type the command for running: " comm
	$comm
	;;

    [kK]ill)
	read -p "Type PID process to kill: " pid
	pkill $pid
	;;

    [eE]xit)
	exit 0
	;;

    help|*)
	echo "You only have to type the options of the menu"
	;;

esac
    
    
