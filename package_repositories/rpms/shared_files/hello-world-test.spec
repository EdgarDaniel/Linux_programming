Summary: Testing hello world
Name: hello-world-test
Version: 1.0.0
Release: 1
URL: http://172.17.0.2
Group: System
License: example 
Packager: Example Team
# Requires: bash mksh pwsh
BuildRoot: /root/example/rpm-work-dir # this should be replaced with your working directory where the spec is saved

%description
An example package containing a hello-world-test binary

%install
mkdir -p %{buildroot}/usr/bin/
cp /root/example/rpm-work-dir/hello-world-test %{buildroot}/usr/bin/hello-world-test

%files
/usr/bin/hello-world-test

%changelog
* Mon Nov 06 2023 alex <alex@earthly.dev>
- initial example

