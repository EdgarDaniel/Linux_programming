import subprocess
import re
import os

escape_chars = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

def runShellCommand(commandToRun, numLines=999):
  proc = subprocess.Popen( commandToRun,cwd=None, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
  while True:
    line = proc.stdout.readline()
    if line:
      if numLines == 1:
        return escape_chars.sub('', line.decode('utf-8').rstrip('\r|\n'))
      else:
        print(escape_chars.sub('', line.decode('utf-8').rstrip('\r|\n')))
    else:
      break


runShellCommand(
"""
commandt="python3"
$commandt -m http.server 8888
""")
