#!/bin/bash

#PROMPT="Introduzca su password: "
#while IFS= read -p "$PROMPT" -r -s -n 1 char; do
#	if [[ $char == $'\0' ]]; then
#		break
#		fi
#		PROMPT='*'
#		SECRET_PASSWD+="$char"
#		done
#		echo

echo "Use of < (stdin)"
echo "Using content of file data.txt"

read -sn 1 -ep "Using cat < data.txt (same 'cat data.txt' )..."

cat < data.txt

read -sn 1 -ep "Using read variable < data.txt (will extract only first line)..."

read  str_data < data.txt

echo -e "The first line of data.txt is \n\t$str_data"

read -sn 1 -ep "Using read -d $'\x04' variable < data.txt (will extract all lines)..."

read -d $'\x04' str_data < data.txt

echo -e "The content of data.txt is \n$str_data"

read -sn 1 -ep "Reading all lines with while and 'done < \${1:-/dev/stdin}'..."

while read line
do
		echo -e "\t\ta) Line: $line"
done < "${1:-/dev/stdin}"
		
read -sn 1 -ep "Reading all lines with while and 'done < data.txt}'..."

while read line
do
		echo -e "\t\tb) Line : $line"
done < data.txt

read -sn 1 -ep "Reading all content with \$(<\"\$thefilename\")'..."

cont=$(<"$1") # is the same as cont=$(cat filename)

echo -e "Content: \n$cont"
