#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/errno.h>

MODULE_LICENSE("GPL");

// Define the original system call function pointer
asmlinkage long (*original_exit)(int);

// Define the new system call function that intercepts the exit syscall
asmlinkage long new_exit(int error_code)
{
    printk(KERN_INFO "Intercepted exit syscall with error code %d\n", error_code);

    // Call the original system call function
    return (*original_exit)(error_code);
}

static int __init init_exit_interceptor(void)
{
    // Look up the address of the original system call function
    original_exit = (void *)sys_call_table[__NR_exit];

    // Disable write protection on the kernel page that contains the system call table
    write_cr0(read_cr0() & (~0x10000));

    // Replace the original system call function with our new function
    sys_call_table[__NR_exit] = (unsigned long *)new_exit;

    // Enable write protection on the kernel page that contains the system call table
    write_cr0(read_cr0() | 0x10000);

    printk(KERN_INFO "Exit interceptor module loaded\n");

    return 0;
}

static void __exit cleanup_exit_interceptor(void)
{
    // Disable write protection on the kernel page that contains the system call table
    write_cr0(read_cr0() & (~0x10000));

    // Restore the original system call function
    sys_call_table[__NR_exit] = (unsigned long *)original_exit;

    // Enable write protection on the kernel page that contains the system call table
    write_cr0(read_cr0() | 0x10000);

    printk(KERN_INFO "Exit interceptor module unloaded\n");
}

module_init(init_exit_interceptor);
module_exit(cleanup_exit_interceptor);
