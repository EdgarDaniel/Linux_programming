#!/bin/bash

echo "Appending strings"

read -p "Type any string " str1
echo "Current str: $str1 -> len(${#str1})"

read -p "Type any string " str2
str1+=" $str2"
echo "Current str: $str1 -> len(${#str1})"

read -sn 1 -ep "Concat/sum integers..."
num=0
str2=-1

while [[ $str2 != 0 ]]; do
	read -p "Type any number (0 for exit) " str2
	((num+=$str2))		
done

echo "The sum is: $num"

	


