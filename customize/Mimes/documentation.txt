Create the xml on database mimes: 
   
   1) On path '/usr/share/mime/packages':
        <?xml version="1.0" encoding="UTF-8"?>
            <mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
               <mime-type type="text/example">
                 <comment>ryj file type / JCompiler </comment>
                 <magic priority="50">
                   <match value="search-string" type="string" offset="10:140"/>
                 </magic>
                 <glob pattern="*.your extension"/>
               </mime-type>
            </mime-info>
            
   2) Update database after that:
            sudo update-mime-database
            
Create the dekstop asociation:

    [Desktop Entry]
    
    Encoding=UTF-8
    Version=1.0
    Icon=(i think is not necessary)
    Type=Application
    NoDisplay=true
    Exec=jcompiler %f
    Name=jcompiler
    Comment=Custom definition for jcompiler

            
Asociate the extension to a desktop

    1) Open file: ~/.config/mimeapps.list
    2) Add in both ([Added Associations],[Default Applications]) 
    theformat/extension = file.desktop;
    Example: text/mss = file.desktop
    
   #text/ryj=userapp-jcompiler-MHVTO0.desktop;
    

