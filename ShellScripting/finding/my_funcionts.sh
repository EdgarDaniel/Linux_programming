function mk_cd {
  mkdir -p $@ && cd ${@:$#}
}

greperr() {
	grep -iRDskip "$*" 2>/dev/null
}

alias lk="lsblk"
alias glab="cd ~/Documents/Gitlab"
alias ghub="cd ~/Documents/Github"
alias ins="sudo dnf install"
alias upg="sudo dnf upgrade"
alias e="$EDITOR"
alias sstt="systemctl status"
alias ssta="sudo systemctl start"
alias ssto="sudo systemctl stop"
alias ssre="sudo systemctl restart"
alias ssen="sudo systemctl enable"
alias ssdi="sudo systemctl disable"

export CDPATH="$HOME/Documents/Gitlab/:$HOME/Documents/Github"


shsc () {

	if [[ $1 == "b" ]]; then
		echo -e '#! /usr/bin/env bash\n' > $2
	elif [[ $1 == "z" ]]; then
		echo -e '#! /usr/bin/env zsh\n' > $2
	fi

	chmod u+x $2 && nvim $2
}

clf () {
	truncate --size=0 "$@"
}
