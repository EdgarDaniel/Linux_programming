#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/syscalls.h>

MODULE_LICENSE("GPL");

// Define the original system call function pointer
asmlinkage long (*original_exit)(int);

// Define the replacement system call function
asmlinkage long new_exit(int error_code)
{
    // Perform any necessary actions here
    printk(KERN_INFO "Intercepted exit system call with error code: %d\n", error_code);
    
    // Call the original system call function
    return (*original_exit)(error_code);
}

// Initialize the module
static int __init init_exit_interceptor(void)
{
    // Get the address of the original system call function
    original_exit = (void*)sys_exit;
    
    // Replace the system call function with our own implementation
    write_cr0(read_cr0() & (~0x10000)); // Disable write protection
    syscalls[__NR_exit] = new_exit; // Replace the function pointer
    write_cr0(read_cr0() | 0x10000); // Enable write protection
    
    printk(KERN_INFO "Exit interceptor module loaded\n");
    
    return 0;
}

// Cleanup the module
static void __exit cleanup_exit_interceptor(void)
{
    // Restore the original system call function
    write_cr0(read_cr0() & (~0x10000)); // Disable write protection
    syscalls[__NR_exit] = original_exit; // Restore the function pointer
    write_cr0(read_cr0() | 0x10000); // Enable write protection
    
    printk(KERN_INFO "Exit interceptor module unloaded\n");
}

//module_init
