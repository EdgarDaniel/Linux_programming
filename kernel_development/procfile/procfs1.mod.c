#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/export-internal.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif


static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x22323136, "single_open" },
	{ 0x92997ed8, "_printk" },
	{ 0x40d31c31, "seq_printf" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0xc1a35b96, "proc_create" },
	{ 0x3ce02ff4, "remove_proc_entry" },
	{ 0x89dfe270, "seq_read" },
	{ 0xf97a4ecd, "seq_lseek" },
	{ 0xa2b0edb1, "single_release" },
	{ 0x8cf78adb, "module_layout" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "C77A525C3A6F674048EC7E2");

MODULE_INFO(suserelease, "openSUSE Tumbleweed");
