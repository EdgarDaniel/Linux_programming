#!/bin/bash

echo "Reading file: $1 with while read_ done < 'file'"

lineNumber=1
while read line
do
    echo -e $lineNumber")\t$line"
    ((lineNumber++))
    
done < $1

echo "Reading with cat"
content=`cat $1`

lineNumber=1
for cnt in $content
do
    echo -e $lineNumber")\t$cnt"
    ((lineNumber++))
done
