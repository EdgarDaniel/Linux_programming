#include <syslog.h>        
#include <stdio.h>
#include <unistd.h>

int main(){
   setlogmask (LOG_UPTO (LOG_NOTICE));
   int i = 0;

   openlog ("exampleprog", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
   while (1){
      syslog (LOG_NOTICE, "Program started by User %d, msg numer %d", getuid (),++i);
      sleep(2);
   }

   closelog ();
   return 0;
}
