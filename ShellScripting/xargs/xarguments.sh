#! /bin/bash

createTestFolder(){
	
	if [[ ! -d "./folderTest" ]] ; then
		echo "Creating folderTest with content..."	
		mkdir folderTest
		(cd ./folderTest && touch file{1..10}.txt && touch file{1..10}.java)
		read -sn 1 -p "Press any key to continue..."
		fi
}

[[ "$1" == "-r" ]] &&  { 
	
	rm -r "./folderTest"
	echo "The test folder has been removed"
} || {
	
	[[ "$1" == "-c" ]] && {
		rm -r "./folderTest"
		createTestFolder
	} || {
	
		if [[ "$1" == "-t" ]]; then
			echo "Creating files with touch"
			echo tfile{1..5}.tch | xargs -p touch
		else
			echo "Finding files .$1 and removing"	
			find "./folderTest" -type f -name "*.$1" | xargs -t rm
		fi		
	}
}


