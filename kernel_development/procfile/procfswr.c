/**
 *  procfs2.c -  create a "file" in /proc
 *
 */

#include <linux/module.h>	/* Specifically, a module */
#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/proc_fs.h>	/* Necessary because we use the proc fs */
#include <asm/uaccess.h>	/* for copy_from_user */
#include <linux/seq_file.h>  // seq_read, ...

#define PROCFS_MAX_SIZE		1024
#define PROCFS_NAME 		"buffer1k"

/**
 * This structure hold information about the /proc file
 *
 */
static struct proc_dir_entry *Our_Proc_File;

/**
 * The buffer used to store character for this module
 *
 */
static char procfs_buffer[PROCFS_MAX_SIZE];

/**
 * The size of the buffer
 *
 */
static size_t procfs_buffer_size = 0;

/** 
 * This function is called then the /proc file is read
 *
 */
static int hello_proc_show(struct seq_file *m, void *v) {

   printk(KERN_INFO "procfile_read (/proc/%s) called\n", PROCFS_NAME);
   seq_printf(m, "Hello procfs!\n");
   return 0;
}

static int hello_proc_open(struct inode *inode, struct  file *file) {
  return single_open(file, hello_proc_show, NULL);
}


/**
 * This function is called with the /proc file is written
 *
 */
static ssize_t write_proc(struct file *filp, const char *buff, size_t len, loff_t * off)
{

      pr_info("proc file wrote.....\n");
      procfs_buffer_size = len;
      if (procfs_buffer_size > PROCFS_MAX_SIZE ) {
      	procfs_buffer_size = PROCFS_MAX_SIZE;
      }
      
      /* write data to the buffer */
      if ( copy_from_user(procfs_buffer, buff, procfs_buffer_size) ) {
      	return -EFAULT;
      }

      return procfs_buffer_size;
  } 

static const struct proc_ops proc_root = {
  .proc_open = hello_proc_open,
  .proc_read = seq_read,
  .proc_write = write_proc,
  .proc_lseek = seq_lseek,
  .proc_release = single_release,
};


/**
 *This function is called when the module is loaded
 *
 */
int init_module()
{
	/* create the /proc file */

	//Our_Proc_File->write_proc = procfile_write;
        proc_create(PROCFS_NAME, 0644, NULL,&proc_root);
	printk(KERN_INFO "/proc/%s created\n", PROCFS_NAME);	
	return 0;	/* everything is ok */
}

/**
 *This function is called when the module is unloaded
 *
 */
void cleanup_module()
{
	remove_proc_entry(PROCFS_NAME, NULL);
	printk(KERN_INFO "/proc/%s removed\n", PROCFS_NAME);
}


MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Reading for /proc/buffer1k ");
