#!/bin/bash

echo "***************FIELDWIDTHS*********************"

echo "Content: " 
cat obj_fieldw.txt
echo -e "\n\n"

awk 'BEGIN{FIELDWIDTHS="3 4 3"}{print $1,$2,$3}' obj_fieldw.txt
