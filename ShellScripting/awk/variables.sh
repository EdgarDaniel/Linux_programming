#!/bin/bash

echo "Variables with awk"

echo "File to cat content:"
cat obj_variables.txt
#content=$(cat obj_variables.txt)
#echo $content

read -sn 1 -ep "Press any key to continue..."


echo -e "\nPrinting numbers in spanish-english without -F..."

awk  '{print $1 " ("$4")" }' obj_variables.txt

read -sn 1 -ep "Press any key to continue..."
echo -e "\nPrinting numbers in spanish-english with -F: ..."
read -sn 1 -ep "Press any key to continue..."

awk -F: '{print "The line is "  $0 " the awk output is: " $1 " is " $2 " lang="$3 }' obj_variables.txt
