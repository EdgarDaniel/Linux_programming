#!/usr/bin/bash

source colors.sh

read -p "Type a path to verify if exists: " path

if [[ -e $path ]]; then
    echo "The path does exist. Its content is: "
    ls $path

    if [[ -f $path ]]; then
	echo "The $path is a regular file"

       if [ ! -s "$path" ]; then
	echo "The file is empty"
       fi

    else
	if [[ -d $path ]]
	then
	    echo "It's a directory"
	fi
    fi

    if [[ -r $path ]]; then
	echo "The file can be readed"
    else
	echo -e "${RED} The file cannot be readed."
    fi

    if [[ -w $path ]]; then
	echo "The file can be written"
    else
	echo -e "${RED} The file cannot be written."
    fi

    if [[ -x  $path ]]; then
	echo "The file can be executed"
    else
	echo -e "${RED} The file cannot be executed."
    fi

    if [[ -O $path ]]; then
	echo "The file its owned by the current user";
    else
	echo "The file its not owned by the current user";
    fi

    if [[ -G $path ]]; then
	echo "The file default group is the same as the current user";
    else
	echo "The file default group is not the same as the current user";
    fi

    # file1 -nt file2 -> file1 is newer than file2
    # file1 -ot file2 -> file1 is older than file2
    # it is neccesary to check if file exists before make that comparisons

else
    echo -e "${RED}The path doesn't exist${NC}"
fi
