#!/bin/zsh

#----------------------------------------
# process B: add 80 to the current balance in a
# non-cooperative way
#----------------------------------------
#./update_balance.sh '30'

#cooperative way:
flock --verbose balance.data ./update_balance.sh '30'
