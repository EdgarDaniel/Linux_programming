#!/bin/zsh

echo "This script run over zsh. It's also works on bash."

file=balance.data
value=$(cat $file)

echo "Current balance: $value"

progress=10

while [[ $progress -lt 101 ]]; do
	
	echo -ne "\033[77DCalculating new balance..$progress%"
	sleep 1
	((progress+=10))
	
done

echo ""

((value+=$1))

echo "Writing new balance ($value) back to $file"
echo $value > "$file"
echo "Done."
	
