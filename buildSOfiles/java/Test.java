public class Test {
        public native void hello();

        static { System.loadLibrary("hi"); }

        public static void main(String[] args){
                Test h = new Test();
                h.hello();
        }
	// JNI with C:
//	https://stackoverflow.com/questions/3950635/how-to-compile-dynamic-library-for-a-jni-application-on-linux
}
