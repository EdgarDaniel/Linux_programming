/*
 *  start.c - Illustration of multi filed modules
 */

#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/module.h>	/* Specifically, a module */

int __init initM(void)
{
	printk(KERN_INFO "Hello, world - this is the kernel speaking\n");
	return 0;
}


module_init(initM);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Edgar Daniel Magallon");
