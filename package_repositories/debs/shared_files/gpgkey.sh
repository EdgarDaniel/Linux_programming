#!/bin/bash

cat << EOF > /tmp/example-pgp-key.batch
%echo Generating an example PGP key
Key-Type: RSA
Key-Length: 4096
Name-Real: example
Name-Email: example@example.com
Expire-Date: 0
%no-ask-passphrase
%no-protection
%commit 
EOF

echo "Generating gpg keys"
export GNUPGHOME="$(mktemp -d ~/example/pgpkeys-XXXXXX)"
gpg --no-tty --batch --gen-key /tmp/example-pgp-key.batch

# info
# gpg --list-keys

gpg --armor --export example > /root/example/pgp-key.public

# info;
# cat ~/example/pgp-key.public | gpg --list-packets

gpg --armor --export-secret-keys example > /root/pgp-key.private

echo "Signing Release File"
export GNUPGHOME="$(mktemp -d ~/example/pgpkeys-XXXXXX)"
cat /root/pgp-key.private | gpg --import
cat /root/example/apt-repo/dists/stable/Release | gpg --default-key example -abs > /root/example/apt-repo/dists/stable/Release.gpg
cat /root/example/apt-repo/dists/stable/Release | gpg --default-key example -abs --clearsign > /root/example/apt-repo/dists/stable/InRelease
