#!/bin/bash

gpg --no-tty --batch --gen-key example-pgp-key.batch
gpg --armor --export example > ./shared_files/pgp-key.public
gpg --armor --export-secret-keys example > ./shared_files/pgp-key.private
