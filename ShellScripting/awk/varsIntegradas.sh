#!/bin/bash

echo "FIELDWIDTHS     especifica el ancho del campo."

echo "RS     Especifica el separador de registros."

echo "FS     Especifica un separador de campos."

echo "OFS  Especifica un separador de Salidas."

echo "ORS  Especifica el separador de Salidas."

read -sn 1 -ep "Continue..."

echo "Using FS=':'; OFS='__<->__'"

awk 'BEGIN{FS=":"; OFS="__<->__"} {print $1,$6,$7}' /etc/passwd

read -sn 1 -ep "Continue..."

echo "Using FS='\n'; RS='\n'"

awk 'BEGIN{ FS="\n"; RS="\n\n"; } {print $1,$2;}' obj_rs_stud.txt

read -sn 1 -ep "Continue..."

awk 'BEGIN{ORS="=";} {print;}' obj_rs_stud.txt
