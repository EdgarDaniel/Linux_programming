import os

import gi

gi.require_version('Nautilus', '3.0')

from gi.repository import Nautilus, GObject

class ColumnExtension(GObject.GObject, Nautilus.MenuProvider):
    def __init__(self):
        pass
    def menu_activate_cb(self, menu, file):
         os.system("gnome-calculator & pid=$!")

    def get_background_items(self, window, file):
        item = Nautilus.MenuItem(name='ExampleMenuProvider::Foo2', 
                                         label='Open calculator', 
                                         tip='',
                                         icon='')
        item.connect('activate', self.menu_activate_cb, file)
        return item,
