#!/bin/bash

echo "Formatos a salidas"
echo "Hexadecimal with awk: "

awk '{

	if ($1 <= 15)
 	{
		printf "Number %d\n",$1
		printf "\t Octal: %o\n",$1
		printf "\t Hex: %x\n",$1
		system("sleep 0.5");
	}
}' numbers.txt

echo "Hexadecimal with mawk (same): "

mawk '{

if ($1 <= 15)
{
printf "Number %d\n",$1
printf "\t Octal: %o\n",$1
printf "\t Hex: %x\n",$1
system("sleep 0.5");
}
}' numbers.txt

#echo 0x12345678 | mawk '{ printf "%s: %x\n", $1, $1 }'
#0x12345678: 12345678

#echo 0x12345678 | gawk '{ printf "%s: %x\n", $1, $1 }'
#0x12345678: 0

#echo 0x12345678 | gawk --non-decimal-data '{ printf "%s: %x\n", $1, $1 }'
#0x12345678: 12345678
