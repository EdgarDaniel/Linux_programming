#!/bin/bash

THIS_PATH=$(dirname $0)
echo "Califs file path dir: $THIS_PATH"

read -sn 1 -ep "If..."

awk '
{
	num = $1

	if ($1 % 2 == 0)
	{
		print "The num: " num " is even"
	}
	else
 	{
 	print "The num: " num " is odd"
 	}
}' "${THIS_PATH}/numbers.txt"

read -sn 1 -ep "While..."

echo "Getting average of: "
cat -n "${THIS_PATH}/califs.txt"
read -sn 1 -ep "Continue..."

awk '{

	name=$1
	sum=0
	i=2
	print "\n\nFields: " NF-1
	
	while(i<=NF)
 	{
 		sum+=$i
 		i++
 	}
 	
 	prom = sum/(NF-1)
 	
 	print "The sum of " name " is " sum
 	print "Their average is " prom
	
	#loop: for (var = 1; var < 5; var++){}

}' "${THIS_PATH}/califs.txt"
