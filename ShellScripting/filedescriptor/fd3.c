#include <unistd.h>

int main(void) {
    write(3, "written in fd 3\n", 16);
    write(2, "written in fd 2\n", 16);
    write(1, "written in fd 1\n", 16);
    write(0, "written in fd 0\n", 16);
}
