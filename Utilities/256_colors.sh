
reds=(1 9 52 88 124 160 196)
blues=(4 6 12 17 18 19 20 21 24 25 26 27 30 31 32 33 38 39 44 45 50 51 74 75 80 81 86 87)
greens=(2 10 28 34 35 36 37 40 41 42 43 46 47 48 49 70 100)
purples=(53 54 55 56 57 60 61 62 63 68 69 89 90 91 92 93 97 98 99)
yellows=(11 172 178 184 202 208 214 215 220 221 226 227 228 229)
blacks=(232 233 234  235)
grays=({236..254})
magentas=({125..129} {132..135} {161..165} {169..171} {197..201} {203..207})

declare -A css=(["reset"]=0 ["resbold"]=21 ["resdim"]=22 ["resundr"]=24 ["resblin"]=25 ["resrev"]=27 ["reshid"]=28 ["bold"]=1 ["dim"]=2 ["ital"]=3 \
		["undrl"]=4 ["blink"]=5 ["invert"]=7 ["hidden"]=8 ["red"]=196 ["dred"]=88 ["lred"]=1 ["blue"]=19 ["dblue"]=17 ["lblue"]=25 \
		["cyan"]=44 ["dcyan"]=32 ["lcyan"]=6 ["green"]=34 ["dgreen"]=28 ["lgreen"]=36 ["purple"]=54 ["dpurple"]=53 ["lpurple"]=56 \
		["yellow"]=184 ["dyellow"]=178 ["lyellow"]=220 ["orange"]=172 ["dorange"]=202 ["lorange"]=214 ["black"]=232 ["dblack"]=233 \
		["lblack"]=234 ["gray"]=238 ["dgray"]=236 ["lgray"]=241 ["white"]=254 ["lwhite"]=247 ["magenta"]=125 ["dmagenta"]=126 ["lmagenta"]=128 \
		["pink"]=165 ["dpink"]=199 ["lpink"]=207)
