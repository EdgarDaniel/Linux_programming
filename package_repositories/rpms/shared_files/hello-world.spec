Summary: A simple program that prints hello world plus paramater
Name: hello-world
Version: 1.0.0
Release: 1
URL: http://172.17.0.2
Group: System
License: example 
Packager: Example Team
Requires: bash hello-world-test
# Requires: bash mksh pwsh
BuildRoot: /root/example/rpm-work-dir # this should be replaced with your working directory where the spec is saved

%description
An example package containing a hello-world binary

%install
mkdir -p %{buildroot}/usr/bin/
cp /root/example/rpm-work-dir/hello-world %{buildroot}/usr/bin/hello-world

%files
/usr/bin/hello-world

%changelog
* Mon Nov 06 2023 alex <alex@earthly.dev>
- initial example

