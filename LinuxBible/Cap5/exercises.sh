#! /bin/zsh

read -k1 "?Exercise 4) Search the /etc dir for every file named passwd. Redirect stderr to /dev/null "
find /etc -name passwd 2> /dev/null

read -k1 "?Exercise 6) Find files in /usr/share/doc dir that have not been modified in more than 300 days"
find /usr/share/doc -type f -mtime +300 -ls

read -k1 "?Exercise 7) Create a /tmp/FILES dir. Find all files under /usr/share that are between 5MB and 10MB and copy to the FILES dir"
mkdir /tmp/FILES
find /usr/share -type f -size +5M -size -10M -exec cp {} /tmp/FILES \;

read -k1 "?Exercise 7) In the /tmp/FILES dir append to each file (making a copy) the word .backup "
find -type f -exec cp {} {}.mybackup \;

read -k1 "?Exercise 8) Find files that have been modified in the last 24 hours"
find /var/log -type f -mtime -1 -ls
