#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char *argv[])
{
   int out = open("/tmp/service.logs", O_RDWR|O_CREAT|O_APPEND, 0600);
   if (-1 == out) { perror("opening /tmp/service.logs"); return 255; }
   
   int save_out = dup(fileno(stdout));

   if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); return 255; }

   printf("Filename: %s\n",argv[0]);
   time_t t = time(NULL);
   struct tm tm = *localtime(&t);
   printf("[%d-%02d-%02d %02d:%02d:%02d] Running script from 'after-cmd' #%s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,argv[1]);
   
   int secs = atoi(argv[1]);
   sleep(secs);

   printf("Script #%s has finished\n",argv[1]);

   fflush(stdout); close(out);

   dup2(save_out, fileno(stdout));
   
   close(save_out);

   puts("back to normal output");

   return 0;
}
