import java.io.*;

public class ReadLogs {
 
static BufferedReader pipeReader;

public static void createBuffer(String namedPipe) throws Exception {
    pipeReader = new BufferedReader(new FileReader(new File(namedPipe)));
} 

public static void process() throws Exception {
   String msg;
    while ((msg = pipeReader.readLine()) != null) {
          System.out.println("Message: "+msg);
    }
}

public static void main(String args[]) throws Exception {
       createBuffer("/home/edgar/Documents/Gitlab/Linux_programming/testing/logsread/pipe1");
	    process();              
            System.out.println("EOF: Finished");
}
}
