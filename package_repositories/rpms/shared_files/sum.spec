Summary: Sum numbers
Name: sum-args
Version: 1.0.0
Release: 1
URL: http://172.17.0.2
Group: System
License: example 
Packager: Example Team
Requires: hello-world-test
BuildRoot: /root/example/rpm-work-dir # this should be replaced with your working directory where the spec is saved

%description
An example package containing a sum-args binary

%install
mkdir -p %{buildroot}/usr/bin/
cp /root/example/rpm-work-dir/sum-args %{buildroot}/usr/bin/sum-args

%files
/usr/bin/sum-args

%changelog
* Mon Nov 06 2023 alex <alex@earthly.dev>
- initial example

