#include <linux/module.h> /*Needed by all modules*/
#include <linux/kernel.h> // Needed for KERN_INFO

/*
 * Tutorial: https://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN40
 *
 * Summary: 
 *	 The __init macro causes the init function to be discarded and its memory freed once the init function finishes for built-in drivers, 
 *	 but not loadable modules. If you think about when the init function is invoked, this makes perfect sense.
 *	 There is also an __initdata which works similarly to __init but for init variables rather than functions.
 *
 *	 The __exit macro causes the omission of the function when the module is built into the kernel, and like __exit, has no effect 
 *	 for loadable modules. Again, if you consider when the cleanup function runs, this makes complete sense; built-in drivers don't need 
 *	 a cleanup function, while loadable modules do.
 *
 * 	 These macros are defined in linux/init.h and serve to free up kernel memory. When you boot your kernel and see something like 
 * 	 Freeing unused kernel memory: 236k freed, this is precisely what the kernel is freeing.
 *
 */

/*
 * This function is executed when the module is loaded by 'insmod,modprobe'
 */
int __init initM(void){
   printk(KERN_ALERT "hello-2.ko [ALERT] Hello World 2.\n");

   //Return 0=module loaded succesfully
   //Return -number: failed to insert module (only prints initM messages)
   //Return number>=1: Works but show warnings
   return 0;
}

/*
 * This function is executed just before removing the module: 'rmmod'
 */
void __exit exitM(void){
   printk(KERN_ALERT "hello-2.ko [ALERT] Goodbye world 2.\n");
}

module_init(initM);
module_exit(exitM);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This is the module 2");
