#!/bin/bash

. ../modules/colors.sh


read -p "Counting to: " num
i=1

while [[ i -le num ]]
do
    printf "${RED}Counting $i${NC}\n"
    ((i++))
    sleep 1
done

while (( i>=0  ))
do
    echo "Reseting... $i"
    ((i--))
    sleep 0.5
done

i2=10

read -n1 -p "Use of until."

until [[ i2 -eq 2 ]]; do
	echo $i2
	((i2--))
done
