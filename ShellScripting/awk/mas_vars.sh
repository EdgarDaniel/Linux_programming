#!/bin/bash

echo "ARGC     Retorna el número de parámetros pasados.!"
echo "ARGV     Retorna los parámetros de la línea de comandos."
echo "ENVIRON     Es un arreglo de las variables de ambiente del shell y sus respectivos valores."
echo "FILENAME    El nombre del archive que está siendo procesado por awk."
echo "NF     Cuenta los campos de la línea que está siendo procesada."
echo "NR    Retorna el total de registros procesados."
echo "FNR     Con esta variable puedes acceder al registro que está siendo procesado."
echo "IGNORECASE     Le dice al programa que ignore las diferencias entre mayúsculas y minúsculas."

echo | awk -v home=$HOME '{print "My home is " home}' 

read -sn 1 -ep "ARCG,ARGV[0],ARGV[1]..."

awk 'BEGIN{print "Nro. argumentos: ", ARGC, "Args: ", ARGV[0], ARGV[1]}'  obj_fieldw.txt

read -sn 1 -ep "ENVIRON['....']"

awk 'BEGIN{ print "Path env: ",ENVIRON["PATH"], "\nShell env: ", ENVIRON["SHELL"] }'


read -sn 1 -ep "Use of \$NF"
awk 'BEGIN{FS=":"; OFS="-->"} {print $1,$0}' /etc/passwd
awk 'BEGIN{FS=":"; OFS=":"} {print $1,$NF}' /etc/passwd

