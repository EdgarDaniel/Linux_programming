#! /bin/python3

import sys
import time
from datetime import datetime

#with open("/tmp/service.logs","a") as o:
#    o.write(f"Filename:  {sys.argv[0]}\\n")
#    o.write(f"[{datetime.now()}] Running after service from py-script  #{sys.argv[1]}\\n")
#    time.sleep(int(sys.argv[1]))

filename = "/tmp/service.logs"
sys.stdout = open(filename,"a")
print(f"Filename:  {sys.orig_argv[1]} with args: {sys.orig_argv[0:]}")
#try:
print(f"[{datetime.now()}] Running after service from py-script  #{sys.argv[1]}")
time.sleep(int(sys.argv[1]))
print(f"Script #{sys.argv[1]} has finished")
#except Exception as e:
#    print(f"[{datetime.now()}] Running after service from py-script  #1")
#    time.sleep(1)
#    print(f"Script #1 has finished")
#    pass
