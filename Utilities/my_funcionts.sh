function mk_cd {
  mkdir -p $@ && cd ${@:$#}
}

weather() { curl -s --connect-timeout 3 -m 5 http://wttr.in/$1; }

logerr(){
	>&2 echo $@
}

greperr() {
	grep -iRDskip "$@" 2>/dev/null
}

export QT_QPA_PLATFORM=xcb

alias bash='bash -l'
alias lk="lsblk"
alias glab="cd ~/Documents/Gitlab"
alias ghub="cd ~/Documents/Github"
alias ins="sudo zypper install"
alias rem="sudo zypper remove"
alias upt="sudo zypper update"
alias zref='sudo zypper ref'
alias upg="sudo zypper dup"
alias zynfo="zypper info"
alias zearch="zypper search"
alias e="nvim"
alias c=clear
alias sstt="systemctl status"
alias ssta="sudo systemctl start"
alias ssto="sudo systemctl stop"
#If the service is stopped, the unit file will be started
alias ssre="sudo systemctl restart"
#Only it's restarted if the unit file is started/running
alias sscre="sudo systemctl condrestart"
alias ssen="sudo systemctl enable"
alias ssdi="sudo systemctl disable"
alias scat="systemctl cat"
alias usstt='systemctl --user status'
alias ussta='systemctl --user start'
alias ussto='systemctl --user stop'
alias ussre='systemctl --user restart'
alias ussdi='systemctl --user disable'
alias ussen='systemctl --user enable'
alias uscat='systemctl --user cat'

alias py="python3"
alias myapps="cd ~/.local/share/applications"
alias sysapps="cd /usr/share/applications"
alias lbin="cd /usr/local/bin"
alias sshagnt='ssh-agent -s > ~/.sshconnections ; sshcon'

if [[ $USER == "edgar" ]]; then
      export CDPATH="$HOME/Documents/Gitlab/:$HOME/Documents/Github"
      export PATH="$PATH:/home/edgarvillanueva/.local/bin:$HOME/.flutterSDK/bin"
fi

is_bash(){
	[[ $SHELL =~ .*bash ]] && return 0 || return 1
}

shsc () {

	if [[ -z $2 ]]; then
		echo "You have to specify a file"
		return 1
	fi

	if [[ -s $2 ]];
	then
		echo "The file $2 already exists."
		if [[ $SHELL =~ .*zsh ]]; then
			read -k1 "r?Do you want to overwrite? (y|Y)"; echo
			regex="y|Y"
		else
			read -n1 -p "Do you want to overwrite? (y|Y) " r;echo
			regex="(y|Y)"
		fi


		if [[ ! $r =~ $regex ]];then
			echo "Exiting..."
			unset regex
			unset r
			return 0
		fi
	fi

	if [[ $1 == "b" ]]; then
		echo -e '#! /usr/bin/env bash\n' > $2
	elif [[ $1 == "z" ]]; then
		echo -e '#! /usr/bin/env zsh\n' > $2
	fi

	unset regex
	unset r
	chmod u+x $2 && nvim $2
}

if is_bash ; then
	export PS1="\[\033[0;31m\]\342\224\214\342\224\200$([[ $? != 0 ]] && echo "[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200")[\[\033[0;39m\]\u\[\033[01;33m\]@\[\033[01;96m\]\h\[\033[0;31m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\$\[\e[0m\]"
fi

clf () {
	truncate --size=0 "$@"
}

alias sshcon="source ~/.sshconnections"
