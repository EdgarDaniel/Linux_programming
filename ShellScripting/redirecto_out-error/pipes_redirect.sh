#!/bin/bash

function hello {

    echo "This is a standar output"
    >&2 echo "This a sterr output"
    #echo "This is a standar output"
}

#########This is the same ###############
#redirect both out and err
#hello &> "stderrpipes.txt"
#hello > "stderrpipes.txt" 2>&1 
#hello >& "stderrpipes.txt"
#hello >out.txt 2>error.txt

#########END###############

#########This is the same ###############

#hello 2>&1 | ( val=$(cat); echo "The val is $val" )
#hello |& ( val=$(cat); echo "The val is $val" )
hello 2>&1 1>/dev/null |  (line=$(cat); echo "Line is: $line" #only redirects error

#########END###############
#hello 2> >( ( line=$(cat); echo "Error Line is: "$line ) )


#hello | ( val=$(cat); echo "The val is $val" ) #Only redirects standar output (sdtout)
