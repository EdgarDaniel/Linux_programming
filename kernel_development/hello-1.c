#include <linux/module.h> /*Needed by all modules*/
#include <linux/kernel.h> // Needed for KERN_INFO

/*
 * Tutorial: https://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN40
 *
 * Summary: 
 * 	> Actually both function can ba called with any name
 * 	> printk(): It is a logging mechanism for the kernel, and is used to log information or give warnings. 
 *	> KERN_INFO: is a macro, is used as well as KERN_WARNING,KERN_ALERT and other macros
 *
 *	> Running: 
 *		* Before building the module, we need to create a file `Makefile` with the necessary content.
 *		* Then in the terminal type `make`
 *		*
 *
 *	> Troubleshoot:
 *		 
 */

/*
 * This function is executed when the module is loaded by 'insmod,modprobe'
 */
int initM(void){
   printk(KERN_INFO "hello-1.ko Hello World 1.\n");

   //Return 0=module loaded succesfully
   //Return -number: failed to insert module (only prints initM messages)
   //Return number>=1: Works but show warnings
   return 0;
}

/*
 * This function is executed just before removing the module: 'rmmod'
 */
void exitM(void){
   printk(KERN_INFO "hello-1.ko Goodbye world 1.\n");
}

module_init(initM);
module_exit(exitM);

/*
 * The following license idents are currently accepted as indicating free
 * software modules
 *
 *	"GPL"				[GNU Public License v2 or later]
 *	"GPL v2"			[GNU Public License v2]
 *	"GPL and additional rights"	[GNU Public License v2 rights and more]
 *	"Dual BSD/GPL"			[GNU Public License v2
 *					 or BSD license choice]
 *	"Dual MIT/GPL"			[GNU Public License v2
 *					 or MIT license choice]
 *	"Dual MPL/GPL"			[GNU Public License v2
 *					 or Mozilla license choice]
 *
 * The following other idents are available
 *
 *	"Proprietary"			[Non free products]
 *
 * There are dual licensed components, but when running with Linux it is the
 * GPL that is relevant so this is a non issue. Similarly LGPL linked with GPL
 * is a GPL combined work.
 *
 * This exists for several reasons
 * 1.	So modinfo can show license info for users wanting to vet their setup 
 *	is free
 * 2.	So the community can ignore bug reports including proprietary modules
 * 3.	So vendors can do likewise based on their own policies
 */
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This is the module 1");
